package PointCounting;
import ij.ImagePlus;
import ij.process.ByteProcessor;
import ij.process.ColorProcessor;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;

import Experiment.Experiment;
import Experiment.PointClass;
import Experiment.SetupDialog;
import Util.TissueFinder;
import Viewer.Field;
import Viewer.Grid;
import Viewer.GridPoint;
import Viewer.Slide;
import Viewer.ViewPort;


public class PointCounting {

	public static Experiment experiment;
	public static int viewPortW = 900;
	public static int viewPortH = 700;
	private static int fieldW = 1800;
	private static int fieldH = 1400;

	private static int macroW = 200;
	private static int fieldCol = 0;
	private static int fieldRow = 0;
	private static int numFields;
	private static int fieldCols;
	private static int fieldRows;
	private static int fieldIndex = 0;
	
	//static int currentSlide = 0;
	private static int currentLevel = 0;
	public static double currentZoom = 0.5;
	
	private static int level0Height;
	private static int level0Width;    
	
	//static Grid pointGrid;
	
	private static int thumbSeries = 1;
	private static double thumbScale;
	private static double thumbDisplayScale;
	private static int thumbH;
	private static int thumbW;
	private static ColorProcessor thumbCp;
	private static ColorProcessor thumbDisplayCp;
	private static ArrayList<Rectangle> thumbFields = new ArrayList<Rectangle>();
	private static ByteProcessor tissueBp;
	
	public static JFrame frame;
	private static JPanel pane;
	private static JPanel countsPane;
	private static JButton openButton;	
	private static JButton nextSlideButton;	
	private static JButton previousSlideButton;
	private static JButton previousFieldButton;
	private static JButton nextFieldButton;
	private static JButton previousFlagButton;
	private static JButton nextFlagButton;
	private static JButton saveExpButton;
	private static JButton loadGridButton;
	private static JButton newGridButton;
	private static JButton writeCSVButton;
	
	public static JButton zoomButton;
	public static JButton pointerButton;
	public static JButton dragButton;
	private static JToolBar toolbar;
	
	private static JLabel slideNavLabel;
	private static JLabel fieldNavLabel;
	private static JLabel flagNavLabel;
	
	private static JLabel slideLabel;
	private static JLabel nameLabel;
	private static JLabel pathLabel;
	private static JLabel countLabels[] = new JLabel[10];
	private static JLabel widthLabel;
	private static JLabel heightLabel;
	private static JLabel pointDistanceLabel;
	private static JLabel pointAreaLabel;
	private static MacroLabel macroLabel;
	private static ViewPort viewport;
	private static JTextArea statusText;
	private static JScrollPane resultsPane;
	private static JScrollPane viewportScrollPane;
	private static JTable resultsTable;
	
	private static SetupDialog setupDialog;

	private static ImagePlus image;
	private static String filePath;
	private static String fileName;
	private static ArrayList<String> fileList = new ArrayList<String>();
	
	private static Slide slide;
	private static File expFile;
	private static File currDir = new File(System.getProperty("user.home"));
	
	public enum Tool {
		POINTER,
		DRAG
	}
	public static Tool currentTool = Tool.POINTER;
	
	public static final Cursor pointerCursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
	public static final Cursor dragCursor = Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);
	
	static private void setup() {
		experiment = new Experiment();
		setupDialog = new SetupDialog(frame,experiment);
		setupDialog.show();
		expFile = setupDialog.getExpFile();
		if (expFile.exists()) {
			experiment.load(expFile);
			//check to see if svs files match
			if (filesMissing()) {
				System.out.println("Files are missing");
				currDir = expFile.getParentFile();
				int response = JOptionPane.showConfirmDialog(frame, "Some SVS files are missing. Do you want to change the directory location?","SVS Files missing",JOptionPane.YES_NO_OPTION);						 
				if (response == JOptionPane.YES_OPTION) {
					JFileChooser fileChooser = new JFileChooser(currDir);
					fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					int result = fileChooser.showOpenDialog(null);
					switch (result) {
						case JFileChooser.APPROVE_OPTION:					
							File newDir = fileChooser.getSelectedFile();
							experiment.setDirectory(newDir);
							for (int i=0;i < experiment.getFileList().size(); i++){
								experiment.setFile(i,new File(newDir.getPath() + File.separator + experiment.getFile(i).getName()));
							}
							for (Slide slide : experiment.getSlides()) {
								slide.setFile(new File(newDir.getPath() + File.separator + slide.getFile().getName()));
							}
							experiment.save(expFile);
							break;
						default:
							System.out.println("The dialog was cancelled or an error occurred. Exiting.");
							System.exit(0);
					}
				} else {	
					System.exit(0);
				}
			currDir = experiment.getDirectory();
			}
			System.out.println("Slide directory: "+experiment.getDirectory());
		} else {
			System.exit(0);
		}
	}
	
	static private boolean filesMissing() {
		boolean missing = false;
		for (File f : experiment.getFileList()){
			if (!f.exists()) {
				missing = true;
				System.out.println(f.getPath() + " not found!");
			}
		}
		return missing;
	}
	
	// static class SvsFilter implements FileFilter {
		// public boolean accepts(File f) {
			// if (f.getPath().endswith(".svs") || f.getPath().endswith(".SVS")) {
			   // return true;
			// }
			// return false;
		// }
	// }
	
	static private void openSlide() {
		thumbFields.clear();
		currentLevel = 1;
		slide = experiment.getCurrentSlide();
		
		String id = slide.getFile().getPath();
		level0Width = slide.getWidth();
		level0Height = slide.getHeight();
		thumbW = slide.getThumbWidth();
		thumbH = slide.getThumbHeight();
		thumbCp = new ColorProcessor(slide.getThumbImage());
		thumbDisplayScale = ((double)macroW)/slide.getWidth();
		TissueFinder tf = new TissueFinder();
		tissueBp = tf.getTissueMask(thumbCp);
		
		for (Field field : slide.getTissueFields()) {
			int x = field.getX();
			int y = field.getY();
			int w = field.getWidth();
			int h = field.getHeight();
			int thumbX = (int)Math.round(x*thumbScale);
			int thumbY = (int)Math.round(y*thumbScale);
			int thumbW = (int)Math.round(w*thumbScale);
			int thumbH = (int)Math.round(h*thumbScale);
			int thumbDisplayX = (int)Math.floor(x*thumbDisplayScale);
			int thumbDisplayY = (int)Math.floor(y*thumbDisplayScale);
			int thumbDisplayW = (int)Math.floor(w*thumbDisplayScale);
			int thumbDisplayH = (int)Math.floor(h*thumbDisplayScale);
			int pixelCount = 0;

			thumbFields.add(new Rectangle(thumbDisplayX,thumbDisplayY,thumbDisplayW,thumbDisplayH));
		}

		// check for saved grid
//		File gridFile = experiment.getGridFileList().get(currentSlide);
		// if (gridFile.exists()) {
			// pointGrid = new Grid(level0Width, level0Height, experiment.getMpp(), experiment.getPointDistance());
			// pointGrid.load(gridFile);
            // viewport.setGrid(pointGrid);
			// pointGrid.unselectAll();
		// } else {
			// newGrid(tissueBp);
		// }
		viewport.setSlide(slide);
		//viewport.setGrid(slide.getGrid());
		updateFlaggedFields();
		updateImageInfo();
		showField();
	}
	
	static public void showField() {
		BufferedImage bi = slide.getCurrentFieldImage();
		viewport.setZoom(currentZoom);
	    viewport.setImage(bi,slide.getCurrentField().getRectangle());
		viewport.update();
		viewportScrollPane.getViewport().setViewPosition(new Point(0,0));
		
	    frame.pack();
		slide.getCurrentField().visit();
		updateImageInfo();
		updateThumbDisplay(slide.getCurrentFieldIndex());
	}
	
	static public void updateFlaggedFields() {
		for (int i=0; i < slide.countTissueFields(); i++) {
			int tissueField = slide.tissueFieldToField(i);
			Field field = slide.getField(tissueField);
			field.unflag();
			for (GridPoint gp : slide.getGrid().getPoints()) {
				if (field.getRectangle().contains(gp.getCenter()) && gp.isFlagged()){
					field.flag();
				}	
			}
		}
	}
	
	static public void updateCountsPane() {
		String[] columnNames = {"Class","Color","Points","Area (microns)","Area %"};	
//		Object[][] data = new Object[viewport.classNames.length][columnNames.length];
		DefaultTableModel model = (DefaultTableModel)resultsTable.getModel();

		for (int i=0; i < columnNames.length; i++) {		
			model.setValueAt(columnNames[i], 0, i);
		}
		Grid grid = slide.getGrid();			
		double tissueArea = grid.getTissueCount() * grid.getPointArea();
		
		for (int i = 0; i < experiment.getPointClasses().size(); i++) {
			PointClass pc = experiment.getPointClass(i);
			double classArea = grid.getCount(i) * grid.getPointArea();
			double areaPercent = classArea/tissueArea;
			model.setValueAt(pc.getName(),i+1,0);
			model.setValueAt(pc.getColorString(),i+1,1);
			model.setValueAt(grid.getCount(i),i+1,2);
			model.setValueAt(classArea,i+1,3);
			if (i == 0) {
				model.setValueAt("NA",i+1,4);
			} else {
				model.setValueAt(areaPercent*100,i+1,4);
			}
		}		
	
		// for (int i=0; i < viewport.classNames.length; i++) {
			// double classArea = viewport.classCounts[i] * slide.getGrid().getPointArea();
			// countLabels[i].setText(viewport.classNames[i]+": "+viewport.classCounts[i]+", "+classArea);
			// countLabels[i].setForeground(viewport.classColors[i]);
			// countLabels[i].setFont(new Font("Sans-serif", Font.BOLD, 18));
			// countLabels[i].setVisible(true);
		// }
	}
		
	static private int getFieldFromMacro(int x, int y) {
	  int hit = -1;
	  for (int i=0; i < thumbFields.size(); i++) {
	    if (thumbFields.get(i).contains(new Point(x,y))) {
		    hit = i;
	    }
	  }
	  return hit;
	}
	
	static private void updateThumbDisplay(int field) {
		// get a bi from the thumbCp, add the overlay update the display in the GUI
		ColorProcessor overlayCp = (ColorProcessor)thumbCp.duplicate().resize(macroW);
		int numPixels = overlayCp.getPixelCount();
		byte[] R = new byte[numPixels];
		byte[] G = new byte[numPixels];
		byte[] B = new byte[numPixels];
		overlayCp.getRGB(R,G,B);
		for (int i=0; i < slide.countTissueFields(); i++) {
			if (slide.getTissueField(i).isVisited()) {
				//System.out.println("visited: "+i);
				int x = (int) thumbFields.get(i).getX();
				int y = (int) thumbFields.get(i).getY();
				int w = (int) thumbFields.get(i).getWidth();
				int h = (int) thumbFields.get(i).getHeight();
				for (int j = y; j < y+h; j++) {
					for (int k = x; k < x+w; k++) { 
						//int index = j*w + k;
						int index = j * overlayCp.getWidth() + k;
						int oldValue = (int)B[index] & 0xff; 
						//int newValue = (oldValue - 127) * 2;
						int newValue = 0;
						B[index] = (byte) newValue; //yellow shift
					}
				}
			}
		}
		overlayCp.setRGB(R,G,B);
		overlayCp.setColor(new Color(0,255,0));
		overlayCp.drawPolygon(rectangleToPolygon(thumbFields.get(field)));
		macroLabel.setIcon(new ImageIcon(overlayCp.getBufferedImage()));
		frame.pack();
	}
	
	static private Polygon rectangleToPolygon(Rectangle rect) {
	  Polygon polyOut = new Polygon();
	  int x = (int)rect.getX();
	  int y = (int)rect.getY();
	  int w = (int)rect.getWidth();
	  int h = (int)rect.getHeight();
	  polyOut.addPoint(x,y);
	  polyOut.addPoint(x+w,y);
	  polyOut.addPoint(x+w,y+h);
	  polyOut.addPoint(x,y+h);		
	  return polyOut;
	}

	static public void updateImageInfo() {
		slide.getFile().getParent();
		int totalSlides = experiment.getFileList().size();
		// update our el-cheapo display
		slideNavLabel.setText("Slide: " + (experiment.getCurrentSlideIndex() + 1) + " / " + totalSlides);
		fieldNavLabel.setText("Field: " + (slide.getCurrentFieldIndex() + 1) + " / " + slide.countTissueFields());
		flagNavLabel.setText("Flag count: " +slide.countFlaggedFields());
		
		nameLabel.setText("image name: "+slide.getFile().getName());
		//pathLabel.setText("path: "+path);
		widthLabel.setText("image width: "+level0Width+ " px");
		heightLabel.setText("image height: "+level0Height+ " px");
		pointDistanceLabel.setText("point distance: "+slide.getGrid().getPointDistance()+ " microns");
		pointAreaLabel.setText("point area: "+slide.getGrid().getPointArea()+ " sq microns");
	}
		 
	private static void createAndShowGUI() {
		JFrame.setDefaultLookAndFeelDecorated(false);

		//Create and set up the window.
		frame = new JFrame("demoBinary");
		//DO_NOTHING_ON_CLOSE so that we can use a listener
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new ExitListener());
		
		previousSlideButton = new JButton(new ImageIcon("icons\\sq_prev_icon&32.png"));
		previousSlideButton.addActionListener(new buttonListener());
		nextSlideButton = new JButton(new ImageIcon("icons\\sq_next_icon&32.png"));
		nextSlideButton.addActionListener(new buttonListener());
		previousFieldButton =  new JButton(new ImageIcon("icons\\sq_prev_icon&32.png"));
		previousFieldButton.addActionListener(new buttonListener());
		nextFieldButton = new JButton(new ImageIcon("icons\\sq_next_icon&32.png"));
		nextFieldButton.addActionListener(new buttonListener());
		previousFlagButton =  new JButton(new ImageIcon("icons\\sq_prev_icon&32.png"));
		previousFlagButton.addActionListener(new buttonListener());
		nextFlagButton = new JButton(new ImageIcon("icons\\sq_next_icon&32.png"));
		nextFlagButton.addActionListener(new buttonListener());
		saveExpButton = new JButton("Save");
		saveExpButton.addActionListener(new buttonListener());
		loadGridButton = new JButton("Load Grid");
		loadGridButton.addActionListener(new buttonListener());
		newGridButton = new JButton("New Grid");
		newGridButton.addActionListener(new buttonListener());
		writeCSVButton = new JButton("WriteCSV");
		writeCSVButton.addActionListener(new buttonListener());
		
		slideLabel = new JLabel("slide number:");
		nameLabel = new JLabel("file:");
		pathLabel = new JLabel("path:");
		widthLabel = new JLabel("width:");
		heightLabel = new JLabel("height:");
		pointDistanceLabel = new JLabel("point distance:");
		pointAreaLabel = new JLabel("point area:");
		
		slideNavLabel = new JLabel("Slide:");
		fieldNavLabel = new JLabel("Field:");
		flagNavLabel = new JLabel("Flag:");
	
		String[] columnNames = {"Class","Color","Points","Area (microns)","Area %"};
		DefaultTableModel model = new DefaultTableModel(8,5) {
			@Override
			public boolean isCellEditable(int row, int column)
				{
					return false;
				}
			};
		
		resultsTable = new JTable(model);
		
		for (int i=0; i < columnNames.length; i++) {		
			model.setValueAt(columnNames[i], 0, i);
		}
		
		for (int i=0; i < countLabels.length; i++) {
			countLabels[i] = new JLabel("Class "+i+": 0");
			countLabels[i].setVisible(false);
		}
		macroLabel = new MacroLabel();
		viewport = new ViewPort();
		//viewport.setPreferredSize(new Dimension(viewPortW,viewPortH));
		viewport.setHorizontalAlignment(JLabel.LEFT);
		viewport.setVerticalAlignment(JLabel.TOP);
		
		pane = new JPanel();
		pane.setLayout(new GridBagLayout());
		JPanel navPane = new JPanel(new GridLayout(0, 3));
		JPanel toolsPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JPanel infoPane = new JPanel(new GridLayout(0, 1));
		countsPane = new JPanel(new GridLayout(0, 1));
		JPanel macroPane = new JPanel(new GridLayout(0, 1));
		
		toolbar = new JToolBar("Toolbar", JToolBar.HORIZONTAL);
		zoomButton = new JButton(new ImageIcon("icons\\level0_icon&32.png"));
		zoomButton.addActionListener(new buttonListener());
		toolbar.add(zoomButton);
		pointerButton = new JButton(new ImageIcon("icons\\cursor_arrow_icon&32.png"));
		pointerButton.addActionListener(new buttonListener());
		toolbar.add(pointerButton);
		dragButton = new JButton(new ImageIcon("icons\\cursor_drag_arrow_icon&32.png"));
		dragButton.addActionListener(new buttonListener());
		toolbar.add(dragButton);
		
		navPane.add(previousSlideButton);
		navPane.add(slideNavLabel);
		navPane.add(nextSlideButton);
		navPane.add(previousFieldButton);
		navPane.add(fieldNavLabel);
		navPane.add(nextFieldButton);
		navPane.add(previousFlagButton);
		navPane.add(flagNavLabel);
		navPane.add(nextFlagButton);
		//navPane.add(newGridButton);
		//navPane.add(saveExpButton);
		toolsPane.add(writeCSVButton);
		toolsPane.add(toolbar);
		//infoPane.add(slideLabel);
		infoPane.add(nameLabel);
		//infoPane.add(pathLabel);
		infoPane.add(widthLabel);
		infoPane.add(heightLabel);
		infoPane.add(pointDistanceLabel);
		infoPane.add(pointAreaLabel);
		countsPane.setMaximumSize(new Dimension(resultsTable.getPreferredSize()));
		countsPane.add(resultsTable);
		for (int i=0; i < countLabels.length; i++) { 
		//	countsPane.add(countLabels[i]);
		}
		macroPane.add(macroLabel);
		Border blackline = BorderFactory.createLineBorder(Color.black);
		pane.setBorder(BorderFactory.createEmptyBorder(
																		10, //top
																		10, //left
																		10, //bottom
																		10) //right
																		);
		navPane.setBorder(BorderFactory.createTitledBorder(blackline, "Navigation"));
		infoPane.setBorder(BorderFactory.createTitledBorder(blackline, "File Info"));
		macroPane.setBorder(BorderFactory.createTitledBorder(blackline, "Macro"));
					
		//set constraints
		GridBagConstraints navC = new GridBagConstraints();
		navC.gridx=0;
		navC.gridy=0;
		navC.anchor=GridBagConstraints.PAGE_START;
		GridBagConstraints toolsC = new GridBagConstraints();
		toolsC.gridx=0;
		toolsC.gridy=1;
		toolsC.anchor=GridBagConstraints.PAGE_START;
		GridBagConstraints infoC = new GridBagConstraints();
		infoC.gridx=0;
		infoC.gridy=2;
		GridBagConstraints countsC = new GridBagConstraints();
		countsC.gridx=0;
		countsC.gridy=3;
		countsC.fill = GridBagConstraints.BOTH ;
		countsC.weightx = 0.5 ;
		countsC.weighty = 0.5 ;
		GridBagConstraints macroC = new GridBagConstraints();
		macroC.gridx=0;
		macroC.gridy=GridBagConstraints.RELATIVE;
		macroC.anchor=GridBagConstraints.PAGE_START;
		macroC.weightx = 1.0 ;
		macroC.weighty = 1.0 ;
		GridBagConstraints viewportC = new GridBagConstraints();
		viewportC.gridx=1;
		viewportC.gridy=0;
		viewportC.gridheight=GridBagConstraints.REMAINDER;
		viewportC.anchor=GridBagConstraints.FIRST_LINE_START;
					
		pane.add(navPane,navC);
		pane.add(toolsPane,toolsC);
		pane.add(infoPane,infoC);
		//pane.add(countsPane,countsC);
				
		pane.add(macroPane,macroC);
		viewportScrollPane = new JScrollPane(viewport);
		viewportScrollPane.setPreferredSize(new Dimension(viewPortW+10,viewPortH+10));
	
		pane.add(viewportScrollPane,viewportC);		
		frame.add(pane);
		//Display the window.
		frame.pack();
		frame.setVisible(true);
		
		JFrame countsFrame = new JFrame();
		countsFrame.add(countsPane);
		countsFrame.pack();
		countsFrame.setVisible(true);
		
	}

	static class ExitListener extends WindowAdapter {
		public void windowClosing(WindowEvent event) {
			//save before exiting?
			int n = JOptionPane.showConfirmDialog(
				frame,
				"Save current grid before exiting?",
				"Confirm New Grid",
				JOptionPane.YES_NO_CANCEL_OPTION);
			if (n == JOptionPane.YES_OPTION) {
				// save and exit
				experiment.save(expFile);
				System.exit(0);
			} else if (n == JOptionPane.NO_OPTION) { 
				System.exit(0);
			}
		}
	}
		
    //Listen to the buttons
	static class buttonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == nextSlideButton) {
				//pointGrid.save(experiment.getGridFileList().get(currentSlide));
				experiment.save(expFile);
				experiment.nextSlide();
				openSlide();
			} else if (e.getSource() == previousSlideButton) {
				//pointGrid.save(experiment.getGridFileList().get(currentSlide));
				experiment.save(expFile);
				experiment.previousSlide();
				openSlide();
			} else if (e.getSource() == previousFieldButton) {
				slide.previousField();
				slide.getGrid().unselectAll();
				showField();
			} else if (e.getSource() == nextFieldButton) {
				slide.nextField();
				slide.getGrid().unselectAll();
				showField();
			} else if (e.getSource() == previousFlagButton) {
				slide.gotoPreviousFlag();
				slide.getGrid().unselectAll();
				showField();
			} else if (e.getSource() == nextFlagButton) {
				slide.gotoNextFlag();
				slide.getGrid().unselectAll();
				showField();
			} else if (e.getSource() == saveExpButton) {
				//slide.getGrid().save();
				experiment.save(expFile);
			} else if (e.getSource() == newGridButton) {
				//confirm this choice
				int n = JOptionPane.showConfirmDialog(
					frame,
					"The current grid will be lost. Proceed?",
					"Confirm New Grid",
					JOptionPane.OK_CANCEL_OPTION);
				if (n == JOptionPane.OK_OPTION) {
					System.out.println("newGrid not implemented");
					//newGrid(tissueBp);
					updateImageInfo();
					showField();
				}
			} else if (e.getSource() == zoomButton) {
				if (currentZoom == 1) {
					currentZoom = 0.5;
					zoomButton.setIcon(new ImageIcon("icons\\level0_icon&32.png"));					
					pointerButton.setSelected(true);
					currentTool = PointCounting.Tool.POINTER;
					frame.setCursor(PointCounting.pointerCursor);
				} else {
					currentZoom = 1;
					zoomButton.setIcon(new ImageIcon("icons\\level1_icon&32.png"));
				}
				updateImageInfo();
				showField();
			} else if (e.getSource() == pointerButton) {
				frame.setCursor(pointerCursor);
				currentTool = Tool.POINTER;
			} else if (e.getSource() == dragButton) {
				frame.setCursor(dragCursor);
				currentTool = Tool.DRAG;
			} else if (e.getSource() == writeCSVButton) {
				//String epoch = "" + System.currentTimeMillis()/1000;
				Calendar now = Calendar.getInstance();
				String basename = expFile.getName().substring(0,expFile.getName().lastIndexOf('.'));
				String dirname = expFile.getParent();
				String year = "" + now.get(Calendar.YEAR);
				String month = String.format("%02d", now.get(Calendar.MONTH)+1);
				String day = String.format("%02d", now.get(Calendar.DAY_OF_MONTH));
				String hour = String.format("%02d", now.get(Calendar.HOUR_OF_DAY));
				String min = String.format("%02d", now.get(Calendar.MINUTE));
				String sec = String.format("%02d", now.get(Calendar.SECOND));
				String timestamp = year+month+day+hour+min+sec;
			
				File csvFile = new File(dirname + File.separator + basename + "_results_"+timestamp+".csv");
				experiment.writeCSV(csvFile);
			} else { 
				System.out.println("button listener not implemented.");
			}
		}
	}
	
    //create a mcroimage class with a mouselistener
	public static class MacroLabel extends JLabel implements MouseListener {	
		public MacroLabel() {
			this.addMouseListener(this);
		}

		public void mouseClicked(MouseEvent e) {
			if (e.getButton() == MouseEvent.NOBUTTON) {
			  System.out.println("No button clicked...");
			} else if (e.getButton() == MouseEvent.BUTTON1) {
				//System.out.println("Click position (X, Y):  " + e.getX() + ", " + e.getY());
				int hit = getFieldFromMacro(e.getX(),e.getY());
				if (hit >= 0) {
					slide.setCurrentField(hit);
					showField();
				}
			} else if (e.getButton() == MouseEvent.BUTTON2) {
			} else if (e.getButton() == MouseEvent.BUTTON3) {
			}
		}
		
		public void mouseExited(MouseEvent e) {
		}

		public void mouseEntered(MouseEvent e) {
		}	
		
		public void mousePressed(MouseEvent e) {
		}		

		public void mouseReleased(MouseEvent e) {
		}		
    }
	
	public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
				setup();
				//createAndShowGUI();
				//openSlide();
            }
        });
    }

}
