package Annotation;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

/**
 * 
 */

/**
 * @author tcornis3
 *
 * Part of the Java Image Processing Cookbook, please see
 * http://www.lac.inpe.br/~rafael.santos/JIPCookbook.jsp
 * for information on usage and distribution.
 * Rafael Santos (rafael.santos@lac.inpe.br)
 * Used under Creative Commons Attribution-Noncommercial-Share Alike 3.0 Unported License
 */

public class NoteAnnotation extends DrawableAnnotation {
	private Point anchor; // Annotation anchor point.
	private int radius; // Annotation radius.
	private String message;
	private ArrayList<String> lines = new ArrayList<String>();
	private Rectangle boundsRect;

	// Constructor for this class.
	public NoteAnnotation(Point a,String m) {
		this.anchor = a;
		this.message = m;
	}
		  
	// Concrete implementation of the paint method.
	public void paint(Graphics2D g2d) {
		int x = anchor.x;
		int y = anchor.y;
		g2d.setColor(getColor());
		g2d.setStroke(getStroke());
		Font font = new Font("Serif", Font.PLAIN, 18);
		g2d.setFont(font);
		// get metrics from the graphics
		FontMetrics metrics = g2d.getFontMetrics(font);
		int widthPx = 200;
		int currChar = 0;
		String s = "";
		for (int i=0; i < this.message.length(); i++) {
			// get the advance of my text in this font and render context
			s = message.substring(currChar,i+1);
			if (metrics.stringWidth(s) >= widthPx) {
				lines.add(s);
				currChar = i;
			}
		}
		lines.add(s);
		
		int hgt = metrics.getHeight();
		int cy = y;
		int h = lines.size() * hgt;
		this.boundsRect = new Rectangle(this.anchor.x,this.anchor.y,widthPx+15,h+10);
		g2d.setPaint(Color.WHITE);
		g2d.fill(boundsRect);
		g2d.setPaint(Color.BLACK);
		g2d.draw(boundsRect); 
		for (String line : lines) {
			//System.out.println(line);
			cy = cy + hgt;
			g2d.drawString(line,x+5,cy);
		}
		
		// get the height of a line of text in this font and render context
		//int hgt = metrics.getHeight();
		// calculate the size of a box to hold the text with some padding.
		//Dimension size = new Dimension(adv+2, hgt+2);
	}
	
	public Rectangle getBoundsRect() {
		return this.boundsRect;
	}
}
