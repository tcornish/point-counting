package Annotation;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 * 
 */

/**
 * @author tcornis3
 *
 * Part of the Java Image Processing Cookbook, please see
 * http://www.lac.inpe.br/~rafael.santos/JIPCookbook.jsp
 * for information on usage and distribution.
 * Rafael Santos (rafael.santos@lac.inpe.br)
 * Used under Creative Commons Attribution-Noncommercial-Share Alike 3.0 Unported License
 */
	public class CrossAnnotation extends DrawableAnnotation {
		private Point center; // Annotation center point.
		private int radius; // Annotation radius.

		// Constructor for this class.
		public CrossAnnotation(Point c,int r) {
			this.center = c;
			this.radius = r;
		}
		
		public void select(Graphics2D g2d) {
			// int x = center.x;
			// int y = center.y;
			// g2d.setColor(Color.BLACK);
			// g2d.setStroke(new BasicStroke(4f));
			// g2d.drawLine(x,y-radius,x,y+radius);
			// g2d.drawLine(x-radius,y,x+radius,y);
			// paint(g2d);
		}
	  
		// Concrete implementation of the paint method.
		public void paint(Graphics2D g2d) {
			int x = center.x;
			int y = center.y;
			g2d.setColor(getColor());
			g2d.setStroke(getStroke());
			g2d.drawLine(x,y-radius,x,y+radius);
			g2d.drawLine(x-radius,y,x+radius,y);
		}  
	}
