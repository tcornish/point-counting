package Annotation;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;


public class Note {

	private Point anchor; // Annotation anchor point.
	private Rectangle boundsRect;
	private int widthPx = 200;
	private Font font = new Font("Serif", Font.PLAIN, 18);
	private String message = "";
	private ArrayList<String> lines = new ArrayList<String>();
	private boolean selected = false;

	public Note(Point a, String m, Graphics2D g) {
		this.anchor = a;
		this.message = m;
		
		int currChar = 0;
		String s = "";
		FontMetrics metrics = g.getFontMetrics(this.font);
		for (int i=0; i < m.length(); i++) {
			//System.out.println("currChar:"+currChar+", i:"+i+", "+m.substring(currChar,i+1));
			s = m.substring(currChar,i+1);
			if (metrics.stringWidth(s) >= this.widthPx) {
				this.lines.add(s);
				currChar = i;
			}
		}
		this.lines.add(s);
	}

	public Rectangle getBoundsRect(Graphics2D g) {	
		FontMetrics metrics = g.getFontMetrics(font);
		int lineHeight = metrics.getHeight();
		int cy = this.anchor.y;
		for (String line : this.lines) {
			cy = cy + lineHeight;
		}
		int rectH = this.lines.size() * lineHeight;
		this.boundsRect = new Rectangle(this.anchor.x,this.anchor.y,this.widthPx+15,rectH+10);		
		
		return this.boundsRect;
	}
	
	public void select() {
		this.selected = true;
	}

	public void deselect() {
		this.selected = false;
	}  
	
	public boolean isSelected() {
		return this.selected;
	}

	public Point getAnchor() {
		return this.anchor;
	}

	public String getMessage() {
		return this.message;
	}
	
	public void setMessage(String m) {
		this.message = m;
	}
	
	public Point getTranslatedAnchor(int x, int y) {
		return new Point(this.anchor.x - x, this.anchor.y - y);
	}
	
}
