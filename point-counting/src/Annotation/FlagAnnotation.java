package Annotation;
import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 * 
 */

/**
 * @author tcornis3
 *
 * Part of the Java Image Processing Cookbook, please see
 * http://www.lac.inpe.br/~rafael.santos/JIPCookbook.jsp
 * for information on usage and distribution.
 * Rafael Santos (rafael.santos@lac.inpe.br)
 * Used under Creative Commons Attribution-Noncommercial-Share Alike 3.0 Unported License
 */
	public class FlagAnnotation extends DrawableAnnotation {
		private Point center; // Annotation center point.
		private int radius; // Annotation radius.

		// Constructor for this class.
		public FlagAnnotation(Point c,int r) {
			this.center = c;
			this.radius = r;
		}
			  
		// Concrete implementation of the paint method.
		public void paint(Graphics2D g2d) {
			float lineWidth = ((BasicStroke)getStroke()).getLineWidth();
			int x = (int)Math.round(center.x - radius - lineWidth);
			int y = (int)Math.round(center.y - radius - lineWidth);
			int h = (int)Math.round((radius + lineWidth)*2);
			int w = (int)Math.round((radius + lineWidth)*2);
			g2d.setColor(getColor());
			g2d.setStroke(getStroke());
			g2d.drawOval(x,y,h,w);
		}  
	}
