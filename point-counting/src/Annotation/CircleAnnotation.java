package Annotation;
import java.awt.Graphics2D;
import java.awt.Point;


/**
 * 
 */

/**
 * @author tcornis3
 *
 * Part of the Java Image Processing Cookbook, please see
 * http://www.lac.inpe.br/~rafael.santos/JIPCookbook.jsp
 * for information on usage and distribution.
 * Rafael Santos (rafael.santos@lac.inpe.br)
 * Used under Creative Commons Attribution-Noncommercial-Share Alike 3.0 Unported License
 */
public class CircleAnnotation extends DrawableAnnotation {
	private Point center; // Annotation center point.
	private int radius; // Annotation radius.

	// Constructor for this class.
	public CircleAnnotation(Point c,int r) {
		center = c;
		radius = r;
	}
  
	// Concrete implementation of the paint method.
	public void paint(Graphics2D g2d) {
		int xmin = (int)(center.x-radius);
		int ymin = (int)(center.y-radius);
		g2d.setColor(getColor());
		g2d.setStroke(getStroke());
		g2d.drawArc(xmin,ymin,2*radius,2*radius,0,360);
	}  
}
