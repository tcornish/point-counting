package Annotation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;

/**
 * 
 */

/**
 * @author tcornis3
 *
*/
public class NoteDialog extends JDialog {
	private JTextArea messageTextArea;
	private JFrame frame;
	private String message = "";
	private boolean okPressed = false;
	private ArrayList<String> messageLines;
		
	public NoteDialog(JFrame parent) {
		super(parent, "Add note", true);
		this.frame = parent;
		
		Box b = Box.createVerticalBox();
		b.add(Box.createGlue());
		messageTextArea = new JTextArea(5,20);
		messageTextArea.setFont(new Font("Serif", Font.PLAIN, 18));
		messageTextArea.setLineWrap(true);
		messageTextArea.setWrapStyleWord(true);
		b.add(messageTextArea);
				
		b.add(Box.createGlue());
		getContentPane().add(b, "Center");

		JPanel p2 = new JPanel();
		JButton ok = new JButton("Ok");
		p2.add(ok);
		JButton cancel = new JButton("Cancel");
		p2.add(cancel);
		getContentPane().add(p2, "South");
		setSize(250,250);
		center();
		
		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				message = messageTextArea.getText();
				okPressed = true;
				setVisible(false);
			}
		});
		
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				okPressed = false;
				setVisible(false);				
			}
		});
	}
	
	public void center() {
		Dimension dim = this.frame.getSize();
		Point loc = this.frame.getLocationOnScreen();
		Dimension size = getSize();

		loc.x += (dim.width - size.width)/2;
		loc.y += (dim.height - size.height)/2;

		if (loc.x < 0) loc.x = 0;
		if (loc.y < 0) loc.y = 0;

		Dimension screen = getToolkit().getScreenSize();

		if (size.width  > screen.width) size.width  = screen.width;
		if (size.height > screen.height) size.height = screen.height;
		if (loc.x + size.width > screen.width) loc.x = screen.width - size.width;
		if (loc.y + size.height > screen.height) loc.y = screen.height - size.height;

		setBounds(loc.x, loc.y, size.width, size.height);
	}	
	
	public boolean getStatus() {
		return this.okPressed;
	}
	
	public String getMessage() {
		return this.message;
	}		
	
	public void getMessageLines() {
		String s = getMessage();
		try {
			int lineCount = messageTextArea.getLineCount();
			for (int i = 0; i < lineCount; i++) {
				System.out.println(s.substring(messageTextArea.getLineStartOffset(i),messageTextArea.getLineEndOffset(i)));
			}
		} catch (BadLocationException b) {
		}
		//return ArrayList<String>;
	}
 
}
