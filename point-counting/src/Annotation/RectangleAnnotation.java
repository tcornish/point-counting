package Annotation;
import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 * 
 */

/**
 * @author tcornis3
 *
 * Part of the Java Image Processing Cookbook, please see
 * http://www.lac.inpe.br/~rafael.santos/JIPCookbook.jsp
 * for information on usage and distribution.
 * Rafael Santos (rafael.santos@lac.inpe.br)
 * Used under Creative Commons Attribution-Noncommercial-Share Alike 3.0 Unported License
 */
	public class RectangleAnnotation extends DrawableAnnotation {
		private Rectangle rect;

		// Constructor for this class.
		public RectangleAnnotation(Rectangle r) {
			this.rect = r;
		}
	  
		// Concrete implementation of the paint method.
		public void paint(Graphics2D g2d) {
			int x = rect.x;
			int y = rect.y;
			int w = rect.width;
			int h = rect.height;
			g2d.setColor(getColor());
			g2d.setStroke(getStroke());
			g2d.drawRect(x,y,w,h);
		}  
	}
