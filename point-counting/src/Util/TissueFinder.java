package Util;
import java.awt.Polygon;
import java.util.ArrayList;

import Viewer.Grid;
import Viewer.GridPoint;
import Viewer.Slide;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.Wand;
import ij.measure.Measurements;
import ij.measure.ResultsTable;
import ij.plugin.ImageCalculator;
import ij.plugin.filter.ParticleAnalyzer;
import ij.process.ByteProcessor;
import ij.process.ColorProcessor;

/**
 * 
 */

/**
 * @author tcornis3
 *
 */
public class TissueFinder {

	public TissueFinder() {
	}
	
	public void assignTissueToClass(Slide slide,int tissueClass) {
		Grid grid = slide.getGrid();
		ByteProcessor tissueBp = getTissueMask(new ColorProcessor(slide.getThumbImage()));
		double thumbScale = slide.getThumbScale();
		ArrayList<Polygon> tissuePolys = tissueToPolygons(thumbScale,tissueBp);
		for (Polygon p : tissuePolys) {
			//System.out.println(p);
			for (GridPoint gp : grid.getPoints()) {
				if ( p.contains(gp.getCenter()) ) {
					gp.setClass(tissueClass);
				}
			}
		}

		ByteProcessor tissueHolesBp = getMaskHoles(tissueBp);
		ArrayList<Polygon> tissueHolePolys = tissueToPolygons(thumbScale,tissueHolesBp);
		for (Polygon p : tissueHolePolys) {
			for (GridPoint gp : grid.getPoints()) {
				if ( p.contains(gp.getCenter()) ) {
					gp.setClass(0);
				}
			}
		}	
	}
	
	public ByteProcessor getTissueMask(ColorProcessor cp) {	
		ByteProcessor bp = (ByteProcessor) cp.convertToByte(false);
		//outline tissue
		ImagePlus tempImp = new ImagePlus("temp",bp);
		//IJ.save(tempImp,"temp1.tif");
		IJ.run(tempImp,"Smooth","");
		//IJ.save(tempImp,"temp2.tif");
		bp = (ByteProcessor) tempImp.getProcessor();
		bp.setAutoThreshold("Otsu", false, bp.NO_LUT_UPDATE);
		
		double minThreshold = bp.getMinThreshold();
		double maxThreshold = bp.getMaxThreshold(); 
		
		// convert to binary
		int[] lut = new int[256];
		for (int j=0; j<256; j++) {
			if (j>=minThreshold && j<=maxThreshold)
				lut[j] = (byte)255;
			else
				lut[j] = 0;
		}
		bp.applyTable(lut);
		tempImp.setProcessor(bp);
		return bp;
	}
	
	private ByteProcessor getMaskHoles(ByteProcessor bp) {
		ImagePlus filledImp = new ImagePlus("filled",bp.duplicate());
		IJ.run(filledImp,"Make Binary","");
		IJ.run(filledImp,"Fill Holes","");
		
		ImagePlus holesImp = new ImagePlus("holes",bp.duplicate());
		IJ.run(holesImp,"Make Binary","");
		
		ImageCalculator ic = new ImageCalculator();
		ImagePlus resultImp = ic.run("Subtract create", filledImp, holesImp);
		//resultImp.show();
			
		return (ByteProcessor) resultImp.getProcessor();
	}
	
	private ArrayList<Polygon> tissueToPolygons(double thumbScale,ByteProcessor tissueBp) {
		ArrayList<Polygon> polys = new ArrayList<Polygon>();
		// use a particle analyzer to find all the particles; convert to mask
		int options = ParticleAnalyzer.SHOW_MASKS + ParticleAnalyzer.RECORD_STARTS + ParticleAnalyzer.SHOW_RESULTS;
		int measurements = Measurements.AREA;
		ResultsTable rt = new ResultsTable();
		// min and max pixel sizes 
		double minSize = 0.0;
		double maxSize = Double.MAX_VALUE;	
		// new particle analyzer
		ParticleAnalyzer pa = new ParticleAnalyzer(options, measurements, rt, minSize, maxSize); 
		// hide the output image to prevent it from automatically being shown
		pa.setHideOutputImage(true);
		// analyze
		tissueBp.setThreshold(255.0,255.0, tissueBp.NO_LUT_UPDATE);
		ImagePlus tissueImp = new ImagePlus("tissue", tissueBp);
		//IJ.save(tissueImp,"tissue.tif");
		pa.analyze(tissueImp);
		// get the processor for the output image (mask) and place it our imagePlus
		tissueBp = (ByteProcessor) (pa.getOutputImage().getProcessor()); 
		tissueImp.setProcessor("tissue",tissueBp);
		rt.show("Results");
		int nResults = rt.getCounter();
		System.out.println("nParticles:" + nResults);
		
		for (int i=0;i < nResults;i++) {
			//perform a wand directly on the tissue mask imageprocessor
			Wand wand = new Wand(tissueBp);
			int x = (int)rt.getValue("XStart", i);
			int y = (int)rt.getValue("YStart", i);
			wand.autoOutline(x,y);
			//get the polygon representing this particle and get its centerpoint
			Polygon poly = new Polygon(wand.xpoints, wand.ypoints, wand.npoints);
			//PolyUtil.printPolygon(PolyUtil.scalePolygon(1/thumbScale,poly));
			polys.add(PolyUtil.scalePolygon(1/thumbScale,poly));
		}
		
		return polys;
	}
	
}
