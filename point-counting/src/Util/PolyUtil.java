package Util;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * 
 */

/**
 * @author tcornis3
 *
 */
public class PolyUtil {

    private PolyUtil() {
        throw new AssertionError();
    }

	static Point2D.Double translatePoint(double dx, double dy,Point2D.Double pointIn) {
		Point2D.Double pointOut = new Point2D.Double();
		pointOut.x = pointIn.x + dx;
		pointOut.y = pointIn.y + dy;
		return pointOut;
	}
	
	static void printPolygon(Polygon poly) {
		String polyString = "";
		for (int i=0; i < poly.npoints; i++) {
			int x = (int) Math.round(poly.xpoints[i]);
			int y = (int) Math.round(poly.ypoints[i]);
			polyString = polyString + x + "," + y + ";";
		}
		System.out.println(polyString);
	}
	
	static Polygon translatePolygon(double dx, double dy,Polygon polyIn) {
		Polygon polyOut = new Polygon();
		for (int i=0; i < polyIn.npoints; i++) {
			int x = (int) Math.round(polyIn.xpoints[i] + dx);
			int y = (int) Math.round(polyIn.ypoints[i] + dy);
			polyOut.addPoint(x,y);
		}
		return polyOut;
	}

	static Polygon scalePolygon(double scale,Polygon polyIn) {
		Polygon polyOut = new Polygon();
		for (int i=0; i < polyIn.npoints; i++) {
			int x = (int) Math.round(polyIn.xpoints[i] * scale);
			int y = (int) Math.round(polyIn.ypoints[i] * scale);
			polyOut.addPoint(x,y);
		}
		return polyOut;
	}

	static Polygon rotatePolygon(double angle,Point2D.Double center,Polygon polyIn) {
		Polygon polyOut = new Polygon();
		angle = Math.toRadians(angle);
		for (int i=0; i < polyIn.npoints; i++) {
			int x = polyIn.xpoints[i];
			int y = polyIn.ypoints[i];
			double cx = center.getX();
			double cy = center.getY();
			int xr = (int) Math.round(cx + (x-cx)*Math.cos(angle) - (y-cy)*Math.sin(angle));
			int yr = (int) Math.round(cy + (x-cx)*Math.sin(angle) + (y-cy)*Math.cos(angle));
			polyOut.addPoint(xr,yr);
		}
		return polyOut;
	}

	static Point2D.Double getCentroid(Polygon poly) {
		double A = getArea(poly);
		double cx = 0;
		double cy = 0;
		for (int i=0; i < poly.npoints; i++) {
		  int j = (i + 1) % poly.npoints;
		  int xi = poly.xpoints[i];
		  int xj = poly.xpoints[j];
		  int yi = poly.ypoints[i];
		  int yj = poly.ypoints[j];

		  double f = ((xi * yj) - (xj * yi));
		  cx += ((xi + xj) * f);
		  cy += ((yi + yj) * f);
		}
		cx /= (6 * A);
		cy /= (6 * A);
		System.out.println(cx+","+cy);
		return new Point2D.Double(cx,cy);
	} 

	static double getArea(Polygon poly) {
		double area = 0;
		for (int i=0; i < poly.npoints; i++) {
			int j = (i + 1) % poly.npoints;
			int xi = poly.xpoints[i];
			int xj = poly.xpoints[j];
			int yi = poly.ypoints[i];
			int yj = poly.ypoints[j];
			area += (xi*yj) - (xj*yi);
		}
		area /=2;
		return area;
	}	

	static Point2D.Double getDistantPoint(Polygon poly) {
		double diameter = 0;
		Point2D.Double center = getCentroid(poly);
		Rectangle bounds = poly.getBounds();
		System.out.println("  nPoints = "+poly.npoints);		
		ArrayList<Point2D.Double> vertices = new ArrayList<Point2D.Double>();
		vertices.add(new Point2D.Double(bounds.getX(),bounds.getY()));
		vertices.add(new Point2D.Double(bounds.getX() + bounds.getWidth(),bounds.getY()));
		vertices.add(new Point2D.Double(bounds.getX() + bounds.getWidth(),bounds.getY() + bounds.getHeight()));
		vertices.add(new Point2D.Double(bounds.getX(),bounds.getY() + bounds.getHeight()));
		Point2D.Double distantPoint = vertices.get(0);
		double cx = center.x;
		double cy = center.y;		
		for (int i=0; i < vertices.size(); i++) {
			Point2D.Double vertex = vertices.get(i);
			double x = vertex.x;
			double y = vertex.y;
			double d = Math.sqrt(Math.pow((cx-x),2)+Math.pow((cy-y),2));
			if (d > diameter) {
				diameter = d;
				//System.out.println("diameter,i: "+diameter+","+i);
				distantPoint = vertex;
			}
		}
		return distantPoint;
	}	

	static double getDiameter(Polygon poly) {
		double diameter = 0;
		Point2D.Double center = getCentroid(poly);
		Rectangle bounds = poly.getBounds();
		System.out.println("  nPoints = "+poly.npoints);		
		ArrayList<Point2D.Double> vertices = new ArrayList<Point2D.Double>();
		vertices.add(new Point2D.Double(bounds.getX(),bounds.getY()));
		vertices.add(new Point2D.Double(bounds.getX() + bounds.getWidth(),bounds.getY()));
		vertices.add(new Point2D.Double(bounds.getX() + bounds.getWidth(),bounds.getY() + bounds.getHeight()));
		vertices.add(new Point2D.Double(bounds.getX(),bounds.getY() + bounds.getHeight()));
		double cx = center.x;
		double cy = center.y;		
		for (int i=0; i < vertices.size(); i++) {
			Point2D.Double vertex = vertices.get(i);
			double x = vertex.x;
			double y = vertex.y;
			double d = Math.sqrt(Math.pow((cx-x),2)+Math.pow((cy-y),2));
			if (d > diameter) {
				diameter = d;
				//System.out.println("diameter,i: "+diameter+","+i);
			}
		}
		return diameter;
	}
	
}
