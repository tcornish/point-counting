package Experiment;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * 
 */

/**
 * @author tcornis3
 *
 */
public class SetupDialog extends JDialog {

	private File inputFolder = new File("");
	private File expFile = new File("");
	private JFrame frame;
	private Experiment experiment;
	private JPanel loadOrNewPanel;
	private JPanel newPanel;
	private JTextField inputFolderField;
	private JTextField expFileField;
	private JButton loadButton;
	private JButton newButton;
	private AbstractButton openButton;
	private AbstractButton expFileButton;
	private JTextField thicknessField;
	private JTextField pointDistanceField;
	private JPanel okCancelPanel;
	private JButton okButton;
	private JButton cancelButton;
	private boolean cancelled = false;

	public SetupDialog(JFrame parent, Experiment e) {
		super(parent, "Load or create experiment", true);
		this.frame = parent;
		this.experiment = e;
		
		loadOrNewPanel = new JPanel();
		loadOrNewPanel.setLayout(new BoxLayout(loadOrNewPanel, BoxLayout.Y_AXIS));
		newPanel = new JPanel();
		newPanel.setLayout(new GridLayout(4,2));
		
		inputFolderField = new JTextField(experiment.getDirectory().getPath());
		inputFolderField.setEditable(false);
		inputFolderField.setPreferredSize(new Dimension(300,30));
		inputFolderField.setAlignmentX(JComponent.RIGHT_ALIGNMENT);

		expFileField = new JTextField(expFile.getPath());
		expFileField.setEditable(false);
		expFileField.setPreferredSize(new Dimension(300,30));
		expFileField.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
		
		loadButton = new JButton("Load an experiment...",new ImageIcon("icons/folder_open_icon&32.png"));
		loadButton.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        loadButton.addActionListener(new ButtonListener());
		newButton = new JButton("Create new experiment",new ImageIcon("icons/doc_new_icon&32.png"));
		newButton.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        newButton.addActionListener(new ButtonListener());
		openButton = new JButton("Select a Folder...",new ImageIcon("icons/folder_open_icon&32.png"));
        openButton.addActionListener(new ButtonListener());
		expFileButton = new JButton("Experiment File...",new ImageIcon("icons/doc_new_icon&32.png"));
        expFileButton.addActionListener(new ButtonListener());
		
		JLabel thicknessLabel = new JLabel("Distance between sections:");
		thicknessLabel.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
		thicknessField = new JTextField(Integer.toString(experiment.getThickness()),6);        		
		thicknessField.addKeyListener(new intKeyAdapter());
		thicknessField.setMaximumSize(new Dimension(200,30));
		thicknessField.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		
		JLabel pointDistanceLabel = new JLabel("Distance between grid points:");	
		pointDistanceField = new JTextField(Integer.toString(experiment.getPointDistance()),6);
		pointDistanceField.addKeyListener(new intKeyAdapter());
		pointDistanceField.setMaximumSize(new Dimension(200,30));
		
		loadOrNewPanel.add(loadButton);
		loadOrNewPanel.add(newButton);
		
		newPanel.add(expFileButton);
		newPanel.add(expFileField);
		newPanel.add(openButton);
		newPanel.add(inputFolderField);
		newPanel.add(thicknessLabel);
		newPanel.add(thicknessField);
		newPanel.add(pointDistanceLabel);
		newPanel.add(pointDistanceField);
		newPanel.setVisible(false);
		
		getContentPane().add(loadOrNewPanel, "North");
		getContentPane().add(newPanel, "Center");
		okCancelPanel = new JPanel();
		okButton = new JButton("Ok");
		okCancelPanel.add(okButton);
		cancelButton = new JButton("Cancel");
		okCancelPanel.add(cancelButton);
		getContentPane().add(okCancelPanel, "South");
		okButton.setEnabled(false);
		
		this.pack();
		this.setResizable(false);

		okButton.addActionListener(new ButtonListener());
		cancelButton.addActionListener(new ButtonListener());
		//setSize(300, 350);
	}
	
	public void update(){
		this.pack();
	}
	
	public void setExpFile(File f) {
		this.expFile = f;
	}
	
	public File getExpFile() {
		return this.expFile;
	}
	
	public boolean wasCancelled() {
		return this.cancelled ;
	}
	
	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == loadButton) {
				JFileChooser fileChooser = new JFileChooser(".");
				fileChooser.setFileSelectionMode( JFileChooser.FILES_ONLY);
				FileNameExtensionFilter filter = new FileNameExtensionFilter("xml","XML");
				fileChooser.addChoosableFileFilter(filter);
				int returnVal = fileChooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					setExpFile(fileChooser.getSelectedFile());
					if (expFile.exists()) {
						setVisible(false);
					}
				}
			} else if (e.getSource() == expFileButton) {
				JFileChooser fileChooser = new JFileChooser(".");
				fileChooser.setFileSelectionMode( JFileChooser.FILES_ONLY);
				FileNameExtensionFilter filter = new FileNameExtensionFilter("xml","XML");
				fileChooser.addChoosableFileFilter(filter);
				int returnVal = fileChooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					setExpFile(fileChooser.getSelectedFile());
					expFileField.setText(expFile.getPath());
				}				
			} else if (e.getSource() == newButton) {
				newPanel.setVisible(true);
				loadOrNewPanel.setVisible(false);
				okCancelPanel.setVisible(true);
				okButton.setEnabled(true);
				update();
			} else if (e.getSource() == openButton) {
				JFileChooser fileChooser = new JFileChooser(".");
				fileChooser.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY);
				fileChooser.showOpenDialog(null);
				inputFolder = fileChooser.getSelectedFile();
				inputFolderField.setText(inputFolder.getPath());
			} else if (e.getSource() == okButton) {
				boolean done = true;
				if (pointDistanceField.getText().equals("")) done = false;
				if (thicknessField.getText().equals("")) done = false;
				if (inputFolderField.getText().equals("")) done = false;
				if (expFile.getName().equals("")) done = false;
			
				if (done) {
					// all fields are populated
					int response = JOptionPane.OK_OPTION;
					if (expFile.exists()) {
						response = JOptionPane.showConfirmDialog(frame, "The file "+expFile.getName()+" exists. If you proceed it will be overwritten","File exists",JOptionPane.OK_CANCEL_OPTION);						
					} 
					if (response == JOptionPane.OK_OPTION) {
						int pointDistance = Integer.parseInt(pointDistanceField.getText());
						int thickness = Integer.parseInt(thicknessField.getText());
						inputFolder = new File (inputFolderField.getText());
						experiment.setDirectory(inputFolder);
						experiment.setThickness(thickness);
						experiment.setPointDistance(pointDistance);
						experiment.readDirectory(inputFolder);
						experiment.save(expFile);
						setVisible(false);
					} else {
						expFile = new File("");
						expFileField.setText(expFile.getPath());
					}
				} else {
					// something is empty
					JOptionPane.showMessageDialog(frame, "Some fields are empty.","Invalid entry",JOptionPane.WARNING_MESSAGE);
				}
			} else if (e.getSource() == cancelButton) {
				cancelled = true;
				setExpFile(new File(""));
				setVisible(false);				
			} else {
				System.out.println("listener not implemented");
			}
		}
	}
	
	
	private class decInputVerifier extends InputVerifier {
		private Pattern regexp;

		public decInputVerifier() {
			this.regexp = Pattern.compile("^\\d+(\\.\\d{1,6})?$");
		}

        public boolean verify(JComponent input) {
			JTextField textField = (JTextField) input;
			Matcher m = regexp.matcher(textField.getText());
			if (m.matches()) {
			//       validationLabel.setText("");
				return true;
			} else {
			//       validationLabel.setText("Field must only contain " + this.validString);
				System.out.println("mismatch");
				getToolkit().beep();
				return false;
			}
		}				
	}
	
	private class intKeyAdapter extends KeyAdapter {
		public void keyTyped(KeyEvent e) {
			char c = e.getKeyChar();
			if (!((c >= '0') && (c <= '9') ||
			 (c == KeyEvent.VK_BACK_SPACE) ||
			 (c == KeyEvent.VK_DELETE))) {
				getToolkit().beep();
				e.consume();
			}
		}
	}
	
	private class decKeyAdapter extends KeyAdapter {
		public void keyTyped(KeyEvent e) {
			char c = e.getKeyChar();
			if (!((c >= '0') && (c <= '9') ||
			 (c == KeyEvent.VK_PERIOD) ||
			 (c == KeyEvent.VK_BACK_SPACE) ||
			 (c == KeyEvent.VK_DELETE))) {
				getToolkit().beep();
				e.consume();
			}
		}
	}
	
}
