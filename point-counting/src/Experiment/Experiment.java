package Experiment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import Util.TissueFinder;
import Viewer.Grid;
import Viewer.Slide;

import com.thoughtworks.xstream.XStream;

public class Experiment {

	private File directory;
	//private ArrayList<String> volumes = new ArrayList<String>();
	private ArrayList<File> fileList = new ArrayList<File>();
	//private ArrayList<File> gridFileList = new ArrayList<File>();
	private int pointDistance = 100;
	private int thickness = 100; 
	static String timestamp = new Date().toString();
	private ArrayList<Slide> slides = new ArrayList<Slide>();
	private int fieldHeight = 1800;
	private int fieldWidth = 1400;
	private int currentSlide;
	private ArrayList<PointClass> pointClasses = new ArrayList<PointClass>();	
	//public int[] classCounts = {0,0,0,0,0,0,0};
	
	// Constructor for this class.
	public Experiment() {
		this.directory = new File("");
		this.pointClasses.add(new PointClass("Deleted","gray",1));
		this.pointClasses.add(new PointClass("Tissue","yellow",1));
		this.pointClasses.add(new PointClass("ADM","cyan",3));
		this.pointClasses.add(new PointClass("Panin-1","green",3));
		this.pointClasses.add(new PointClass("Panin-2","magenta",3));
		this.pointClasses.add(new PointClass("Panin-3","red",3));
		this.pointClasses.add(new PointClass("Cancer","blue",3));
	}
	
	public ArrayList<PointClass> getPointClasses() {
		return this.pointClasses;
	}

	public int countPointClasses() {
		return this.pointClasses.size();
	}
	
	public PointClass getPointClass(int i) {
		return this.pointClasses.get(i);
	}
	
	public void writeCSV(File csvFile) {
		try {
			FileWriter writer = new FileWriter(csvFile);			
			double pointArea = this.pointDistance * this.pointDistance;
			//write header
			writer.append("Slide");
			writer.append(',');
			writer.append("area_per_pt");
			writer.append(',');
			writer.append("total_pts");
			writer.append(',');
			for (PointClass pc : pointClasses) {
				writer.append(pc.getName() + "_points");
				writer.append(',');
				writer.append(pc.getName() + "_area_sqmicrons");
				writer.append(',');
				writer.append(pc.getName() + "_area_%");
				writer.append(',');
				writer.append(pc.getName() + "_tissue_%");
				writer.append(',');
			}
			writer.append('\n');
			//write one line per slide
			for(Slide slide : this.slides) {
				Grid grid = slide.getGrid();
				writer.append(slide.getFileName());
				writer.append(',');
				writer.append("" + pointArea);
				writer.append(',');
				writer.append("" + grid.countPoints());
				writer.append(',');
				int tissueCount = grid.getTissueCount();
				//write class results
				for (int i = 0; i < grid.getClassCounts().size(); i++) {
					int classCount = grid.getCount(i);
					double classArea = classCount * pointArea;
					double areaPercent = (classCount/(double)grid.countPoints()) * 100.0d;
					double tissuePercent = (classCount/(double)tissueCount) * 100.0d;
					writer.append("" + classCount);
					writer.append(',');
					writer.append("" + classArea);
					writer.append(',');
					writer.append("" + areaPercent);
					writer.append(',');
					if (i == 0) {
						writer.append("NA");
					} else {
						writer.append("" + tissuePercent);
					}
					writer.append(',');
				}
				writer.append('\n');
			}		
			//flush to file and close
			writer.flush();
			writer.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		} 
    }
	
	
	public String getTimestamp() {
		return this.timestamp;
	}

	public void readDirectory(File d) {
		this.directory = d;
		this.fileList.clear();
		//this.gridFileList.clear();
		File[] listOfFiles = this.directory.listFiles(); 
		for (File f : listOfFiles) {
			if (f.isFile()) {
				if (f.getName().endsWith(".svs") || f.getName().endsWith(".SVS")) {
					addFile(f);
					addSlide(f,fieldHeight,fieldWidth);
				}
			}	
		}
	}

	public void setDirectory(File d) {
		this.directory = d;
	}

	public File getDirectory() {
		return this.directory;
	}
	
	public void addFile(File f) {
		this.fileList.add(f);
		//this.gridFileList.add(new File(f.getPath().substring(0, f.getPath().lastIndexOf('.')) + ".xml"));
	}

	public void addSlide(File f, int fh, int fw) {
		Slide slide = new Slide(f,fh,fw);
		Grid grid = new Grid(slide.getWidth(),slide.getHeight(),slide.getMpp(),pointDistance,pointClasses.size());
		File gridFile = new File(f.getPath().substring(0, f.getPath().lastIndexOf('.')) + ".xml");
		grid.setFile(gridFile);
		slide.setGrid(grid);
		TissueFinder tf = new TissueFinder();
		tf.assignTissueToClass(slide,1);
		this.slides.add(slide);
	}
	
	public Slide getSlide(int i) {
		return this.slides.get(i);
	}

	public Slide getCurrentSlide() {
		return getSlide(currentSlide);
	}

	public int getCurrentSlideIndex() {
		return currentSlide;
	}
	
	public void setCurrentSlide(int i) {
		this.currentSlide = i;
	}
	
	public int countSlides() {
		return this.slides.size();
	}
	
	public void nextSlide() {
		this.currentSlide++;
		if (this.currentSlide == countSlides()) {
			this.currentSlide = 0;
		}
	}

	public void previousSlide() {
		this.currentSlide--;
		if (this.currentSlide < 0) {
			this.currentSlide = countSlides() - 1;
		}
	}
	
	public ArrayList<Slide> getSlides() {
		return this.slides;
	}

	public void setSlides(ArrayList<Slide> s) {
		this.slides = s;
	}
	
	public int getSlideCount() {
		return this.slides.size();
	}
	
	public ArrayList<File> getFileList() {
		return this.fileList;
	}

	public void setFileList(ArrayList<File> f) {
		this.fileList = f;
	}
	
	public File getFile(int i) {
		return this.fileList.get(i);
	}
	
	public void setFile(int i, File f) {
		this.fileList.set(i,f);
	}
	
	public void save(File file) {
		XStream xstream = new XStream();
		System.out.println("Saving experiment setup to "+file+"...");
        try {
            FileOutputStream fs = new FileOutputStream(file);
            xstream.toXML(this,fs);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }		
		System.out.println("Saved.");
	}
	
	public void load(File file) {
		XStream xstream = new XStream();
		Experiment temp;
		System.out.println("Loading experiment setup from "+file+"...");
        try {
            FileInputStream fs = new FileInputStream(file);
            temp = (Experiment)xstream.fromXML(fs);
			this.setDirectory(temp.getDirectory());
			this.setPointDistance(temp.getPointDistance());
			this.setThickness(temp.getThickness());
			this.setFileList(temp.getFileList());
			this.setSlides(temp.getSlides());
			System.out.println("slidecount: "+temp.getSlideCount());
			this.setTimestamp(temp.getTimestamp());
			this.setCurrentSlide(temp.getCurrentSlideIndex());
			this.setFieldHeight(temp.getFieldHeight());
			this.setFieldWidth(temp.getFieldWidth());
		} catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
		System.out.println("Loaded.");
	}

	public void setFieldHeight(int fh) {
		this.fieldHeight = fh;
	}

	public int getFieldHeight() {
		return this.fieldHeight;
	}
	
	public void setFieldWidth(int fw) {
		this.fieldWidth = fw;
	}

	public int getFieldWidth() {
		return this.fieldWidth;
	}
	
	public void setTimestamp(String s) {
		this.timestamp = s;
	}

	public void setPointDistance(int pd) {
		this.pointDistance = pd;
	}

	public int getPointDistance() {
		return this.pointDistance;
	}

	public void setThickness(int t) {
		this.thickness = t;
	}

	public int getThickness() {
		return this.thickness;
	}	
		
}
