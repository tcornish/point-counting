package Experiment;
import java.awt.Color;
import java.util.HashMap;
	
/*
 */

 public class PointClass {
	private String name;
	private String color;
	private float weight;

	public PointClass(String n,String c,float w) {
		this.name = n;
		this.color = c;
		this.weight = w;
	}
	
	public String getColorString() {
		return this.color;
	}
	
	public Color getColor() {
		HashMap<String, Color> colorMap = mapColors();
		return (Color)colorMap.get(this.color);
	}
	
	public String getName() {
		return this.name;
	}

	public float getWeight() {
		return this.weight;
	}
		
	private HashMap<String, Color> mapColors() {
		HashMap<String, Color> colorMap = new HashMap<String, Color>();
		colorMap.put("gray", Color.LIGHT_GRAY);
		colorMap.put("yellow",Color.YELLOW);
		colorMap.put("cyan",Color.CYAN);
		colorMap.put("green",Color.GREEN);
		colorMap.put("magenta",Color.MAGENTA);
		colorMap.put("red",Color.RED);
		colorMap.put("blue",Color.BLUE);
		colorMap.put("orange",Color.ORANGE);
		colorMap.put("black",Color.BLACK);
		return colorMap;
	}
}