package Viewer;
import java.util.ArrayList;
import java.util.Random;

import java.awt.Rectangle;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.Graphics2D;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;

import Annotation.Note;

import com.thoughtworks.xstream.XStream;

public class Grid {
	private int pointDistance; //microns
	private double pointDistancePx; //pixels	
	private double pointArea; //sq microns
	private double mpp; //microns per pixel
	private int cols;
	private int rows;
	private int gridWidth; //pixels (= image width)
	private int gridHeight; //pixels (= image height)
	private Point2D.Double gridOrigin; //microns
	private ArrayList<GridPoint> gridPoints = new ArrayList<GridPoint>(); //pixels
	private ArrayList<Note> notes = new ArrayList<Note>(); //pixels
	private int radius = 20;
	private double radiusScale = 0.25;
	private File file;
	private ArrayList<Integer> classCounts = new ArrayList<Integer>();
	private int tissueCount = 0;
	
	public Grid(int gw, int gh, double m, int pd, int cc) {
		this.gridWidth = gw;
		this.gridHeight = gh;
		this.pointDistance = pd;
		this.mpp = m;
		this.pointDistancePx = pd / mpp;
		this.pointArea = this.pointDistance * this.pointDistance;
		this.radius = (int)Math.round(this.pointDistancePx * radiusScale);
		// the grid origin is randomly placed between 0,0 and pointDPx/2,pointDPx/2
		Random rand = new Random();
		this.gridOrigin = new Point2D.Double(rand.nextDouble()*(this.pointDistancePx/2),rand.nextDouble()*(this.pointDistancePx/2));

		this.cols = (int)Math.ceil((double)gridWidth/(double)this.pointDistancePx);
		this.rows = (int)Math.ceil((double)gridHeight/(double)this.pointDistancePx);

		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				int x = (int) Math.round( (col*this.pointDistancePx) + this.gridOrigin.x);
				int y = (int) Math.round( (row*this.pointDistancePx) + this.gridOrigin.y);
				this.gridPoints.add(new GridPoint(new Point(x,y)));
			}
		}
		for (int i = 0; i < cc; i++) {
			classCounts.add(0);
		}
	}
	
	public void selectPoints(Rectangle selectionRect) {	
		// select points in the rectangle
		for (int i=0; i < gridPoints.size(); i++) {
			if (selectionRect.contains(gridPoints.get(i).getCenter())) {
				gridPoints.get(i).select();
			}
		}
	}

	public ArrayList<GridPoint> getSelectedPoints() {	
		// return all the selected points
		ArrayList<GridPoint> selectedPoints = new ArrayList<GridPoint>();
		for (GridPoint gp : gridPoints) {
			if (gp.isSelected()) selectedPoints.add(gp);
		}
		return selectedPoints;
	}
	
	public void unselectAll() {
		for (GridPoint gp : gridPoints) {
			gp.deselect();
		}
	}
	
	public ArrayList<GridPoint> getPoints() {	
		return this.gridPoints;
	}

	public void addNote(Point a, String m, Graphics2D g) {	
		this.notes.add(new Note(a,m,g));
	}

	public void removeNote(int i) {	
		this.notes.remove(i);
	}
	
	public ArrayList<Note> getNotes() {	
		return this.notes;
	}
	
	public double getMpp() {
		return this.mpp;
	}
	
	public void setMpp(double m) {
		this.mpp = m;
	}
	
	public int getRadius() {
		return this.radius;
	}
	
	public void setRadius(int r) {
		this.radius = r;
	}
	
	public int getGridWidth() {
		return this.gridWidth;
	}
	
	public void setGridWidth(int gw) {
		this.gridWidth = gw;
	}

	public int getGridHeight() {
		return this.gridHeight;
	}
	
	public void setGridHeight(int gh) {
		this.gridHeight = gh;
	}

	public int getPointDistance() {
		return this.pointDistance;
	}
	
	public void setPointDistance(int pd) {
		this.pointDistance = pd;
	}
	
	public double getPointDistancePx() {
		return this.pointDistancePx;
	}
	
	public void setPointDistancePx(double pdp) {
		this.pointDistancePx = pdp;
	}
	
	public Point2D.Double getGridOrigin() {
		return this.gridOrigin;
	}
	
	public void setGridOrigin(Point2D.Double p) {
		this.gridOrigin = p;
	}
	
	public int getCols() {
		return this.cols;
	}
	
	public void setCols(int c) {
		this.cols = c;
	}
	
	public int getRows() {
		return this.rows;
	}
	
	public void setRows(int r) {
		this.rows = r;
	}

	public double getPointArea() {
		return this.pointArea;
	}
	
	public void updateCounts() {
		int tc = 0;
		for (int i = 0; i < classCounts.size(); i++) {
			int count = 0;
			for (GridPoint gp : gridPoints) {
				if (gp.getPointClass() == i) count++;
			}
			this.classCounts.set(i,count);
			if (i > 0) {
				tc = tc + count;
			}
		}
		this.tissueCount = tc;
	}
	
	public int getTissueCount() {
		return this.tissueCount;
	}

	public int countPoints() {
		return this.gridPoints.size();
	}
	
	public int getCount(int c) {
		return this.classCounts.get(c);
	}

	public ArrayList<Integer> getClassCounts() {
		return this.classCounts;
	}
	
	public int getSelectedCount() {
		int count = 0;
		for (GridPoint gp : gridPoints) {
			if (gp.isSelected()) count++;
		}
		return count;
	}

	public File getFile() {
		return this.file;
	}
	
	public void setFile(File f) {
		this.file = f;
	}
	
	public void save() {
		XStream xstream = new XStream();
		System.out.println("Saving grid to "+this.file+"...");
        try {
            FileOutputStream fs = new FileOutputStream(this.file);
            xstream.toXML(this,fs);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }		
		System.out.println("Saved.");
	}

	public void load(File file) { //not fully implemented
		XStream xstream = new XStream();
		System.out.println("Loading grid from "+file+"...");
		Grid temp;
        try {
            FileInputStream fs = new FileInputStream(file);
            temp = (Grid)xstream.fromXML(fs);
			this.gridWidth = temp.getGridWidth();
			this.gridHeight = temp.getGridHeight();
			this.pointDistance = temp.getPointDistance();
			this.pointDistancePx = temp.getPointDistancePx();
			this.pointArea = temp.getPointArea();
			this.radius = temp.getRadius();
			this.mpp = temp.getMpp();
			this.gridOrigin = temp.getGridOrigin();
			this.cols = temp.getCols();
			this.rows = temp.getRows();
			this.gridPoints = temp.getPoints();
			this.notes = temp.getNotes();
			this.file = temp.getFile();
			this.tissueCount = temp.getTissueCount();
			this.classCounts = temp.getClassCounts();
		} catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }		
		System.out.println("Loaded.");

	}	
}