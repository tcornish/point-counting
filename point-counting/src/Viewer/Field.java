package Viewer;
import java.awt.Rectangle;

/**
 * 
 */

/**
 * @author tcornis3
 *
 */
public class Field {
	private Rectangle fieldRect;
	private boolean visited;
	private boolean flagged;
	private boolean selected;
	
	public Field(Rectangle r) {
		this.fieldRect = r;
		this.visited = false;
		this.flagged = false;
	}
	
	public Rectangle getRectangle() {
		return this.fieldRect;
	}
	
	public int getHeight() {
		return this.fieldRect.height;
	}

	public int getWidth() {
		return this.fieldRect.width;
	}

	public int getX() {
		return this.fieldRect.x;
	}

	public int getY() {
		return this.fieldRect.y;
	}
	
	public void setRectangle(Rectangle r) {
		this.fieldRect = r;
	}
	
	public void flag() {
		this.flagged = true;
	}

	public void unflag() {
		this.flagged = false;
	}
	
	public boolean isFlagged() {
		return this.flagged;
	}
	
	public void visit() {
		this.visited = true;
	}

	public void unvisit() {
		this.visited = false;
	}
	
	public boolean isVisited() {
		return this.visited;
	}
	
	public void select() {
		this.selected = true;
	}

	public void unselect() {
		this.selected = false;
	}
	
	public boolean isSeleced() {
		return this.selected;
	}
}
