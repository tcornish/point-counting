package Viewer;
import java.awt.Point;


public class GridPoint {
	private Point center; // Annotation center point.
	private int pointClass = 0;
	private boolean selected = false;
	private boolean flagged = false;

	// Constructor for this class.
	public GridPoint(Point c) {
		this.center = c;
	}

	public void flag() {
		this.flagged = true;
	}

	public void unflag() {
		this.flagged = false;
	}
	
	public void select() {
		this.selected = true;
	}

	public void deselect() {
		this.selected = false;
	}  
	
	public void setClass(int c) {
		this.pointClass = c;
	}
	
	public int getPointClass() {
		return this.pointClass;
	}
	
	public boolean isSelected() {
		return this.selected;
	}

	public boolean isFlagged() {
		return this.flagged;
	}
	
	public Point getCenter() {
		return this.center;
	}

	public Point getTranslatedCenter(int x, int y) {
		return new Point(this.center.x - x, this.center.y - y);
	}

	public Point getScaledCenter(double scale) {
		return new Point((int)Math.round(this.center.x * scale), (int)Math.round(this.center.y * scale));
	}
	
}
